from pydantic import BaseModel
from models import IssueIn, IssueOut, IssueUpdateOut
from queries.pool import pool


class IssueQueries(BaseModel):
    def create_issue(self, issue: IssueIn) -> IssueOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO issues (
                        title,
                        description,
                        resolved,
                        account_id,
                        game_id
                        )
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING issue_id
                    """,
                    [
                        issue.title,
                        issue.description,
                        issue.resolved,
                        issue.account_id,
                        issue.game_id,
                    ],
                )
                id = result.fetchone()[0]
                return IssueOut(
                    issue_id=id,
                    title=issue.title,
                    description=issue.description,
                    resolved=issue.resolved,
                    account_id=issue.account_id,
                    game_id=issue.game_id,
                )

    def get_all_issues(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT *
                    FROM issues
                    """
                )
                rows = result.fetchall()
                return [self.record_to_issue_out(row) for row in rows]

    def get_issues_for_game(self, game_id: int) -> list[IssueOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT *
                    FROM issues
                    WHERE game_id = %s
                    """,
                    [game_id],
                )
                rows = result.fetchall()
                return [self.record_to_issue_out(row) for row in rows]

    def get_one_issue(self, issue_id: int) -> IssueOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT *
                    FROM issues
                    WHERE issue_id = %s
                    """,
                    [issue_id],
                )
                record = result.fetchone()
                return self.record_to_issue_out(record)

    def record_to_issue_out(self, record):
        return IssueOut(
            issue_id=record[0],
            title=record[1],
            description=record[2],
            resolved=record[3],
            account_id=record[4],
            game_id=record[5],
        )

    def delete(self, issue_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM issues
                        WHERE issue_id = %s
                        """,
                        [issue_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, issue_id: int, title: str, description: str, resolved: bool
    ) -> IssueUpdateOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE issues
                    SET title=%s, description=%s, resolved=%s
                    WHERE issue_id = %s
                    RETURNING issue_id, title, description, resolved
                    """,
                    [title, description, resolved, issue_id],
                )
                issue = cur.fetchone()
                return IssueUpdateOut(
                    issue_id=issue[0],
                    title=issue[1],
                    description=issue[2],
                    resolved=issue[3],
                )
