from pydantic import BaseModel
from models import AccountIn, AccountOutWithHashedPassword, AccountUpdateOut
from queries.pool import pool


class DuplicateAccountError(ValueError):
    pass


class AccountUpdateError(ValueError):
    pass


class AccountQueries(BaseModel):
    def get_one(self, account_name: str) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT *
                    FROM accounts
                    WHERE account_name = %s
                    """,
                    [account_name],
                )
                record = result.fetchone()
                return self.record_to_account_out(record)

    def record_to_account_out(self, record):
        return AccountOutWithHashedPassword(
            account_id=record[0],
            account_name=record[1],
            hashed_password=record[2],
            biography=record[3],
            links=record[4],
            rank=record[5],
            issues=record[6],
            comments=record[7],
        )

    def create(
        self, account: AccountIn, hashed_password: str
    ) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO accounts (
                        account_name,
                        hashed_password,
                        biography,
                        links,
                        rank,
                        issues,
                        comments
                        )
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING account_id
                    """,
                    [
                        account.account_name,
                        hashed_password,
                        account.biography,
                        account.links,
                        account.rank,
                        account.issues,
                        account.comments,
                    ],
                )
                id = result.fetchone()[0]
                return AccountOutWithHashedPassword(
                    account_id=id,
                    account_name=account.account_name,
                    hashed_password=hashed_password,
                    biography=account.biography,
                    links=account.links,
                    rank=account.rank,
                    issues=account.issues,
                    comments=account.issues,
                )

    def delete(self, account_name: str) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM accounts
                        WHERE account_name = %s
                        """,
                        [account_name],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, account_id: int, biography: str, links: str
    ) -> AccountUpdateOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE accounts
                    SET biography = %s,
                        links = %s
                    WHERE account_id = %s
                    RETURNING account_id, biography, links
                    """,
                    (biography, links, account_id),
                )
                account = cur.fetchone()
                if not account:
                    raise AccountUpdateError("Failed to update account.")
                return AccountUpdateOut(
                    account_id=account[0],
                    biography=account[1],
                    links=account[2],
                )
