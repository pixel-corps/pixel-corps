from pydantic import BaseModel
from models import CommentIn, CommentOut, CommentUpdateOut
from queries.pool import pool


class CommentQueries(BaseModel):
    def create_comment(self, comment: CommentIn) -> CommentOut | None:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO comments (comment_text, issue_id, account_id)
                    VALUES (%s, %s, %s)
                    RETURNING comment_id
                    """,
                    [
                        comment.comment_text,
                        comment.issue_id,
                        comment.account_id,
                    ],
                )
                if cur is not None:
                    id = cur.fetchone()[0]
                    return CommentOut(
                        comment_id=id,
                        comment_text=comment.comment_text,
                        issue_id=comment.issue_id,
                        account_id=comment.account_id,
                    )
                else:
                    print("There was no id.")

    def get_all_comments(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT *
                    FROM comments
                    """
                )
                rows = result.fetchall()
                return [self.record_to_comment_out(row) for row in rows]

    def get_all_comments_for_issue(self, issue_id: int) -> list[CommentOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT *
                    FROM comments
                    WHERE issue_id = %s
                    """,
                    [issue_id],
                )
                rows = result.fetchall()
                return [self.record_to_comment_out(row) for row in rows]

    def get_one_comment(self, comment_id: int) -> CommentOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT *
                    FROM comments
                    WHERE comment_id = %s
                    """,
                    [comment_id],
                )
                record = result.fetchone()
                return self.record_to_comment_out(record)

    def record_to_comment_out(self, record):
        return CommentOut(
            comment_id=record[0],
            comment_text=record[1],
            issue_id=record[2],
            account_id=record[3],
        )

    def delete_comment(self, comment_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM comments
                        WHERE comment_id = %s
                        """,
                        [comment_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self,
        comment_id: int,
        comment_text: str,
    ) -> CommentUpdateOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE comments
                    SET comment_text=%s
                    WHERE comment_id = %s
                    RETURNING issue_id, comment_text
                    """,
                    [comment_text, comment_id],
                )
                issue = cur.fetchone()
                return CommentUpdateOut(
                    comment_id=issue[0], comment_text=issue[1]
                )
