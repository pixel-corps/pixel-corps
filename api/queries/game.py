from pydantic import BaseModel
from models import GameIn, GameOut, GameUpdateOut
from queries.pool import pool


class GameQueries(BaseModel):
    def get_all(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT *
                    FROM games
                    """
                )
                rows = result.fetchall()
                return [self.record_to_game_out(row) for row in rows]

    def get_one(self, game_id: int) -> GameOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT *
                    FROM games
                    WHERE game_id = %s
                    """,
                    [game_id],
                )
                record = result.fetchone()
                return self.record_to_game_out(record)

    def record_to_game_out(self, record):
        return GameOut(
            game_id=record[0],
            name=record[1],
            genre=record[2],
            account_id=record[3],
        )

    def create_game(self, game: GameIn) -> GameOut | None:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO games (name, genre, account_id)
                    VALUES (%s, %s, %s)
                    RETURNING game_id
                    """,
                    [
                        game.name,
                        game.genre,
                        game.account_id,
                    ],
                )
                id = result.fetchone()[0]
                return GameOut(
                    game_id=id,
                    name=game.name,
                    genre=game.genre,
                    account_id=game.account_id,
                )

    def delete(self, game_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM games
                        WHERE game_id = %s
                        """,
                        [game_id],
                    )
                    return True
        except Exception:
            return False

    def update(self, game_id: int, name: str, genre: str) -> GameUpdateOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE games
                    SET name=%s, genre=%s
                    WHERE game_id = %s
                    RETURNING game_id, name, genre
                    """,
                    [name, genre, game_id],
                )
                game = cur.fetchone()
                return GameUpdateOut(
                    game_id=game[0], name=game[1], genre=game[2]
                )
