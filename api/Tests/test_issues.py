from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.issue import IssueQueries


client = TestClient(app)


class MockIssueQueries:
    def get_one_issue(self, issue_id: int):
        return {
            "issue_id": 2,
            "title": "test_issue",
            "description": "testing this description",
            "resolved": True,
            "account_id": 4,
            "game_id": 2,
        }


def fake_get_issue_data():
    return {
        "issue_id": 2,
        "title": "test_issue",
        "description": "testing this description",
        "resolved": True,
        "account_id": 4,
        "game_id": 2,
    }


def test_get_issue():
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_issue_data
    )
    app.dependency_overrides[IssueQueries] = MockIssueQueries
    issue_id = 2
    response = client.get(f"/api/issues/{issue_id}")
    assert response.status_code == 200
    issues = response.json()
    assert len(issues) == 6
    assert response.json() == {
        "issue_id": 2,
        "title": "test_issue",
        "description": "testing this description",
        "resolved": True,
        "account_id": 4,
        "game_id": 2,
    }
    app.dependency_overrides = {}
