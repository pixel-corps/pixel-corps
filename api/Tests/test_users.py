from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.account import AccountQueries


client = TestClient(app)


class MockAccountQueries:
    def get_one(self, account_name: str):
        return {
            "account_id": 100,
            "account_name": "test_name",
            "hashed_password": "test_password",
            "biography": "test_bio",
            "links": "test_link",
            "rank": 1,
            "issues": "test_issue",
            "comments": "test_comment",
        }


def fake_get_account_data():
    return {
        "account_id": 100,
        "account_name": "test_name",
        "hashed_password": "test_password",
        "biography": "test_bio",
        "links": "test_link",
        "rank": 1,
        "issues": "test_issue",
        "comments": "test_comment",
    }


def test_get_account():
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_account_data
    )
    app.dependency_overrides[AccountQueries] = MockAccountQueries
    account_name = "test_name"
    response = client.get(f"/api/accounts/{account_name}")
    assert response.status_code == 200
    accounts = response.json()
    assert len(accounts) == 8
    assert response.json() == {
        "account_id": 100,
        "account_name": "test_name",
        "hashed_password": "test_password",
        "biography": "test_bio",
        "links": "test_link",
        "rank": 1,
        "issues": "test_issue",
        "comments": "test_comment",
    }
    app.dependency_overrides = {}
