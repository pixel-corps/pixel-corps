from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.comment import CommentQueries


client = TestClient(app)


class MockCommentQueries:
    def get_one_comment(self, comment_id: int):
        return {
            "comment_id": 2,
            "comment_text": "test_comment",
            "issue_id": 3,
            "account_id": 4,
        }


def fake_get_comment_data():
    return {
        "comment_id": 2,
        "comment_text": "test_comment",
        "issue_id": 3,
        "account_id": 4,
    }


def test_get_comment():
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_comment_data
    )
    app.dependency_overrides[CommentQueries] = MockCommentQueries
    comment_id = 2
    response = client.get(f"/api/comments/{comment_id}")
    assert response.status_code == 200
    comments = response.json()
    assert len(comments) == 4
    assert response.json() == {
        "comment_id": 2,
        "comment_text": "test_comment",
        "issue_id": 3,
        "account_id": 4,
    }
    app.dependency_overrides = {}
