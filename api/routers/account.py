from fastapi import (
    APIRouter,
    Request,
    Response,
    Depends,
    HTTPException,
    status,
)
from models import (
    AccountToken,
    AccountIn,
    AccountForm,
    AccountOut,
    AccountUpdateIn,
    AccountOutWithHashedPassword,
)
from queries.account import (
    AccountQueries,
    DuplicateAccountError,
    AccountUpdateError,
)
from authenticator import authenticator
from typing import Optional


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    try:
        if account and authenticator.cookie_name in request.cookies:
            return AccountToken(
                access_token=request.cookies[authenticator.cookie_name],
                token_type="Bearer",
                account=AccountOut(**account),
            )
    except Exception:
        raise HTTPException(status_code=400, detail="Cannot retrieve token")


@router.post("/api/accounts", response_model=AccountToken)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = repo.create(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with these credentials",
        )
    form = AccountForm(username=info.account_name, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    accountout = AccountOut(
        account_id=account.account_id,
        account_name=account.account_name,
        biography=account.biography,
        links=account.links,
        rank=account.rank,
        comments=account.comments,
        issues=account.issues,
    )
    return AccountToken(account=accountout, **token.dict())


@router.delete("/api/accounts/{account_name}", response_model=bool)
def delete_account(
    account_name: str,
    accounts: AccountQueries = Depends(),
) -> bool:
    return accounts.delete(account_name)


@router.get(
    "/api/accounts/{account_name}",
    response_model=Optional[AccountOutWithHashedPassword],
)
def get_account(account_name: str, accounts: AccountQueries = Depends()):
    try:
        return accounts.get_one(account_name)
    except Exception:
        raise HTTPException(status_code=400, detail="Account not found")


@router.put("/api/accounts/myprofile")
async def update_account(
    info: AccountUpdateIn,
    account=Depends(authenticator.try_get_current_account_data),
    repo: AccountQueries = Depends(),
):
    try:
        updated_account = repo.update(
            account.get("account_id"), info.biography, info.links
        )
        return updated_account
    except AccountUpdateError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Failed to update account.",
        )
