from fastapi import APIRouter, Depends, HTTPException
from models import IssueIn, IssueOut, IssueUpdateIn, CommentOut
from queries.issue import IssueQueries
from queries.comment import CommentQueries
from typing import Optional

router = APIRouter()


@router.post("/api/issues", response_model=Optional[IssueOut])
def create_issue(info: IssueIn, repo: IssueQueries = Depends()):
    try:
        issue = repo.create_issue(info)
        return issue
    except Exception:
        raise HTTPException(
            status_code=400, detail="Could not create an issue"
        )


@router.get("/api/issues")
def get_all_issues(issues: IssueQueries = Depends()):
    try:
        return issues.get_all_issues()
    except Exception:
        raise HTTPException(status_code=400, detail="Could not get all issues")


@router.get("/api/issues/{issue_id}", response_model=Optional[IssueOut])
def get_one_issue(issue_id: int, issues: IssueQueries = Depends()):
    try:
        return issues.get_one_issue(issue_id)
    except Exception:
        raise HTTPException(
            status_code=400, detail="Issue with id does not exist"
        )


@router.get(
    "/api/issues/{issue_id}/comments",
    response_model=Optional[list[CommentOut]],
)
def get_comments_for_issue(
    issue_id: int, comments: CommentQueries = Depends()
):
    try:
        return comments.get_all_comments_for_issue(issue_id)
    except Exception:
        raise HTTPException(
            status_code=400,
            detail="Could not get comments for the issue given",
        )


@router.delete("/api/issues/{issue_id}", response_model=bool)
def delete_issue(issue_id: int, issues: IssueQueries = Depends()) -> bool:
    try:
        return issues.delete(issue_id)
    except Exception:
        raise HTTPException(status_code=400, detail="Could not delete issue")


@router.put("/api/issues/{issue_id}")
async def update_issue(
    issue_id: int, info: IssueUpdateIn, repo: IssueQueries = Depends()
):
    try:
        updated_issue = repo.update(
            issue_id, info.title, info.description, info.resolved
        )
        return updated_issue
    except Exception:
        raise HTTPException(status_code=400, detail="Could not update issue")
