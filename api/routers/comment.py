from fastapi import APIRouter, Depends, HTTPException
from models import CommentIn, CommentOut, CommentUpdateIn
from queries.comment import CommentQueries
from typing import Optional


router = APIRouter()


@router.post("/api/comments", response_model=Optional[CommentOut])
def create_comment(info: CommentIn, repo: CommentQueries = Depends()):
    try:
        comment = repo.create_comment(info)
        return comment
    except Exception:
        raise HTTPException(status_code=400, detail="Cannot create comment")


@router.get("/api/comments")
def get_all_comments(comments: CommentQueries = Depends()):
    try:
        return comments.get_all_comments()
    except Exception:
        raise HTTPException(status_code=400, detail="Cannot get all comments")


@router.get("/api/comments/{comment_id}", response_model=Optional[CommentOut])
def get_one_comment(comment_id: int, comments: CommentQueries = Depends()):
    try:
        return comments.get_one_comment(comment_id)
    except Exception:
        raise HTTPException(status_code=400, detail="Comment does not exist")


@router.delete("/api/comments/{comment_id}", response_model=bool)
def delete_comment(
    comment_id: int, comments: CommentQueries = Depends()
) -> bool:
    try:
        return comments.delete_comment(comment_id)
    except Exception:
        raise HTTPException(status_code=400, detail="Could not delete comment")


@router.put("/api/comments/{comment_id}")
async def update_comment(
    comment_id: int, info: CommentUpdateIn, repo: CommentQueries = Depends()
):
    try:
        updated_comment = repo.update(
            comment_id,
            info.comment_text,
        )
        return updated_comment
    except Exception:
        raise HTTPException(status_code=400, detail="Could not update comment")
