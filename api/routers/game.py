from fastapi import APIRouter, Depends, HTTPException
from models import GameOut, GameIn, GameUpdateIn, IssueOut
from queries.issue import IssueQueries
from typing import Optional
from queries.game import GameQueries

router = APIRouter()


@router.get("/api/games")
def get_all_game(games: GameQueries = Depends()):
    try:
        return games.get_all()
    except Exception:
        raise HTTPException(status_code=400, detail="Could not get all games")


@router.get("/api/games/{game_id}", response_model=Optional[GameOut])
def get_one_game(game_id: int, games: GameQueries = Depends()):
    try:
        return games.get_one(game_id)
    except Exception:
        raise HTTPException(status_code=400, detail="Invalid game id")


@router.get(
    "/api/games/{game_id}/issues", response_model=Optional[list[IssueOut]]
)
def get_issue_for_game(game_id: int, games: IssueQueries = Depends()):
    try:
        return games.get_issues_for_game(game_id)
    except Exception:
        raise HTTPException(
            status_code=400, detail="Could not get issue for game"
        )


@router.post("/api/games", response_model=Optional[GameOut])
def create_game(info: GameIn, repo: GameQueries = Depends()):
    try:
        game = repo.create_game(info)
        return game
    except Exception:
        raise HTTPException(status_code=400, detail="Could not create game")


@router.delete("/api/games/{game_id}", response_model=bool)
def delete_game(game_id: int, games: GameQueries = Depends()) -> bool:
    try:
        return games.delete(game_id)
    except Exception:
        raise HTTPException(status_code=400, detail="Could not delete game")


@router.put("/api/games/{game_id}")
async def update_game(
    game_id: int, info: GameUpdateIn, repo: GameQueries = Depends()
):
    try:
        updated_game = repo.update(game_id, info.name, info.genre)
        return updated_game
    except Exception:
        raise HTTPException(status_code=400, detail="Could not update game")
