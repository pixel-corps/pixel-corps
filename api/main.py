import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
from routers import account, game, issue, comment

app = FastAPI()
app.include_router(authenticator.router, tags=["Auth"])
app.include_router(account.router, tags=["Auth"])
app.include_router(game.router, tags=["Games"])
app.include_router(issue.router, tags=["Issues"])
app.include_router(comment.router, tags=["Comments"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }
