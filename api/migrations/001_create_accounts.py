steps = [
    [
        """
        CREATE TABLE accounts (
            account_id SERIAL PRIMARY KEY NOT NULL,
            account_name VARCHAR(255) UNIQUE NOT NULL,
            hashed_password VARCHAR(255) NOT NULL,
            biography TEXT NULL,
            links VARCHAR(255) NULL,
            rank BIGINT NULL,
            issues VARCHAR(255) NULL,
            comments TEXT NULL
        );
        """,
        """
        DROP TABLE accounts;
        """,
    ]
]
