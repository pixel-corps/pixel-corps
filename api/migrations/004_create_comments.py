steps = [
    [
        """
        CREATE TABLE comments (
            comment_id SERIAL PRIMARY KEY NOT NULL,
            comment_text TEXT NOT NULL,
            issue_id BIGINT NOT NULL,
            account_id BIGINT NOT NULL,
            CONSTRAINT fk_accounts FOREIGN KEY (account_id) REFERENCES accounts(account_id),
            CONSTRAINT fk_issues FOREIGN KEY (issue_id) REFERENCES issues(issue_id)
        );
        """,
        """
        DROP TABLE comments;
        """,
    ]
]
