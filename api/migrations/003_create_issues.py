steps = [
    [
        """
        CREATE TABLE issues (
            issue_id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(255) NOT NULL,
            description TEXT NOT NULL,
            resolved BOOLEAN NULL,
            account_id BIGINT NOT NULL,
            game_id BIGINT NOT NULL,
            CONSTRAINT fk_accounts FOREIGN KEY (account_id) REFERENCES accounts(account_id),
            CONSTRAINT fk_games FOREIGN KEY (game_id) REFERENCES games(game_id)
        );
        """,
        """
        DROP TABLE issues;
        """,
    ]
]
