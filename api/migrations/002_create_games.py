steps = [
    [
        """
        CREATE TABLE games (
            game_id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(255) NOT NULL,
            genre VARCHAR(255) NOT NULL,
            account_id BIGINT NOT NULL,
            CONSTRAINT fk_accounts FOREIGN KEY (account_id) REFERENCES accounts(account_id)
        );
        """,
        """
        DROP TABLE games;
        """,
    ]
]
