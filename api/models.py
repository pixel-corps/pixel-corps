from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from typing import Optional


class Account(BaseModel):
    account_name: str
    biography: Optional[str]
    links: Optional[str]
    rank: Optional[int]
    comments: Optional[str]
    issues: Optional[str]


class AccountIn(Account):
    password: str


class AccountOut(Account):
    account_id: int


class AccountUpdateIn(BaseModel):
    biography: str
    links: str


class AccountUpdateOut(BaseModel):
    account_id: int
    biography: str
    links: str


class AccountInWithInfo(BaseModel):
    account_name: str
    password: str
    biography: str
    links: str
    rank: int
    comments: str
    issues: str


class AccountOutWithInfo(BaseModel):
    account_name: str
    hashed_password: str
    biography: str
    links: str
    rank: int
    comments: str
    issues: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class AccountOutWithHashedPassword(Account):
    account_id: int
    hashed_password: str


class GameIn(BaseModel):
    name: str
    genre: str
    account_id: int


class GameOut(BaseModel):
    game_id: int
    name: str
    genre: str
    account_id: int


class GameUpdateIn(BaseModel):
    name: str
    genre: str


class GameUpdateOut(BaseModel):
    game_id: int
    name: str
    genre: str


class IssueIn(BaseModel):
    title: str
    description: str
    resolved: bool
    account_id: int
    game_id: int


class IssueOut(BaseModel):
    issue_id: int
    title: str
    description: str
    resolved: bool
    account_id: int
    game_id: int


class IssueUpdateOut(BaseModel):
    issue_id: int
    title: str
    description: str
    resolved: bool


class IssueUpdateIn(BaseModel):
    title: str
    description: str
    resolved: bool


class CommentIn(BaseModel):
    comment_text: str
    issue_id: int
    account_id: int


class CommentOut(BaseModel):
    comment_id: int
    comment_text: str
    issue_id: int
    account_id: int


class CommentUpdateIn(BaseModel):
    comment_text: str


class CommentUpdateOut(BaseModel):
    comment_id: int
    comment_text: str
