import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.account import AccountQueries
from models import AccountOut, AccountOutWithHashedPassword


class PixelAuthenticator(Authenticator):
    async def get_account_data(
        self,
        account_name: str,
        accounts: AccountQueries,
    ):
        return accounts.get_one(account_name)

    def get_account_getter(
        self,
        accounts: AccountQueries = Depends(),
    ):
        return accounts

    def get_hashed_password(self, account: AccountOutWithHashedPassword):
        return account.hashed_password

    def get_account_data_for_cookie(
        self, account: AccountOutWithHashedPassword
    ):
        return account.account_name, AccountOut(**account.dict())


authenticator = PixelAuthenticator(os.environ["SIGNING_KEY"])
