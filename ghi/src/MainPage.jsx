import { Outlet, Link } from 'react-router-dom'
import { useGetAllIssuesQuery } from './app/apiSlice'

function MainPage() {
    const { data: issues } = useGetAllIssuesQuery()
    return (
        <div className="text-blue-500 hover:text-blue-800">
            <div>
                <section className="bg-gray-800 text-white py-16">
                    <div className="container mx-auto text-center">
                        <h2 className="text-3xl font-bold mb-4">About Us</h2>
                        <p className="text-lg">
                            Welcome to our vibrant community, where indie game
                            developers come together to share expertise, seek
                            advice, and find inspiration for their projects. Our
                            platform serves as a hub for developers at all
                            stages of their journey, offering a supportive
                            environment where they can collaborate, troubleshoot
                            technical issues, and navigate the intricacies of
                            game development. Whether you are a seasoned
                            developer or just starting out, join us and become
                            part of a community dedicated to fostering
                            creativity and innovation in the indie game
                            development scene.
                        </p>
                    </div>
                </section>
                <Outlet />
                <div className="bg-gray-900 text-white min-h-screen">
                            <div className="max-w-3xl mx-auto p-4">
                                <h1 className="text-3xl font-bold text-center mb-8">Latest Issues</h1>
                                <div className="grid grid-cols-1 gap-6">
                                    {issues && [...issues].sort((a,b) => b.issue_id - a.issue_id).map((issue) => {
                                        return (
                                    <div key={issue.issue_id} className="bg-gray-800 border border-gray-700 rounded-lg shadow-lg transition duration-300 ease-in-out transform hover:shadow-xl hover:scale-105">
                                        <Link to={`/issue/${issue.issue_id}`} className="block p-4">
                                            <h2 className="text-2xl font-bold text-blue-500 hover:text-blue-700 mb-2">{issue.title}</h2>
                                            <p className="text-gray-400 mb-2">{issue.description}</p>
                                        </Link>
                                    </div>
                                    )
                                })}
                                </div>
                            </div>
                </div>


            </div>
            <footer className="bg-gray-800 text-white py-8">
                <div className="container mx-auto text-center">
                    <p>
                        &copy; {new Date().getFullYear()} Pixel Network. All
                        rights reserved.
                    </p>
                    <ul className="flex justify-center space-x-4 mt-4">
                        <li>
                            <a
                                href="#"
                                className="text-blue-500 hover:text-blue-800"
                            >
                                Privacy Policy
                            </a>
                        </li>
                        <li>
                            <a
                                href="#"
                                className="text-blue-500 hover:text-blue-800"
                            >
                                Terms of Service
                            </a>
                        </li>
                    </ul>
                </div>
            </footer>
        </div>
    )
}
export default MainPage
