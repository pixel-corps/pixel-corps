import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './MainPage'
import CreateAccount from './components/account/createAccount'
import Login from './components/account/login'
import CreateGame from './components/game/create_game'
import CreateComment from './components/comment/create_comment'
import CreateIssue from './components/issue/create_issue'
import GameList from './components/game/list_games'
import IssueList from './components/issue/list_issues'
import Nav from './Nav'
import IssuePage from './components/issue/issue_page'
import GamePage from './components/game/game_page'
import EditComment from './components/comment/edit_comment'
import EditIssue from './components/issue/edit_issue'
console.table(import.meta.env)

/**
 *
 * @typedef {{module: number, week: number, day: number, min: number, hour: number}} LaunchInfo
 * @typedef {{launch_details: LaunchInfo, message?: string}} LaunchData
 *
 * @returns {React.ReactNode}
 */
const API_HOST = import.meta.env.VITE_API_HOST

function App() {
    const domain = /https:\/\/[^/]+/
    const basename = import.meta.env.VITE_PUBLIC_URL.replace(domain, '')
    if (!API_HOST) {
        throw new Error()
    }
    return (
        <BrowserRouter basename={basename}>
            <Nav />
            <div>
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="/account/signup" element={<CreateAccount />} />
                    <Route path="/account/login" element={<Login />} />
                    <Route path="/game/create" element={<CreateGame />} />
                    <Route path="/game" element={<GameList />} />
                    <Route
                        path="/issue/create/:game_id"
                        element={<CreateIssue />}
                    />
                    <Route path="/issue" element={<IssueList />} />
                    <Route
                        path="/comment/create/:issue_id"
                        element={<CreateComment />}
                    />
                    <Route path="/issue/:issue_id" element={<IssuePage />} />
                    <Route path="/game/:game_id" element={<GamePage />} />
                    <Route
                        path="/issue/:issue_id/comment/:comment_id/edit"
                        element={<EditComment />}
                    />
                    <Route
                        path="/issue/:issue_id/edit"
                        element={<EditIssue />}
                    />
                </Routes>
            </div>
        </BrowserRouter>
    )
}

export default App
