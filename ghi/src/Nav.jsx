import { NavLink } from 'react-router-dom'
import { useGetTokenQuery, useLogoutMutation } from './app/apiSlice'
import Logo from './images/logo/Logo.png'

const Nav = () => {
    const { data: account } = useGetTokenQuery()
    const [logout] = useLogoutMutation()

    return (
        <nav className="bg-gray-900 text-white">
            <div className="flex items-center justify-between px-4 py-2">
                <NavLink
                    to="/"
                    className="text-2xl font-semibold tracking-wider uppercase hover:text-blue-300"
                >
                    <img src={Logo} />
                </NavLink>
                <ul className="flex space-x-4">
                    {!account && (
                        <>
                            <li>
                                <NavLink
                                    to="/account/signup"
                                    className="hover:text-blue-300"
                                >
                                    Sign Up
                                </NavLink>
                            </li>
                            <li>
                                <NavLink
                                    to="/account/login"
                                    className="hover:text-blue-300"
                                >
                                    Log In
                                </NavLink>
                            </li>
                        </>
                    )}
                    {account && (
                        <>
                            <li>
                                <NavLink
                                    to="/game/create"
                                    className="hover:text-blue-300"
                                >
                                    Create a Game
                                </NavLink>
                            </li>
                            <li>
                                <NavLink
                                    to="/game"
                                    className="hover:text-blue-300"
                                >
                                    All Games
                                </NavLink>
                            </li>
                            <li>
                                <NavLink
                                    to="/issue"
                                    className="hover:text-blue-300"
                                >
                                    All Issues
                                </NavLink>
                            </li>
                            <li>
                                <button
                                    onClick={() => logout()}
                                    className="bg-red-600 hover:bg-red-700 text-white font-semibold py-2 px-4 rounded-full focus:outline-none focus:ring-2 focus:ring-red-300"
                                >
                                    Logout
                                </button>
                            </li>
                        </>
                    )}
                </ul>
            </div>
        </nav>
    )
}

export default Nav
