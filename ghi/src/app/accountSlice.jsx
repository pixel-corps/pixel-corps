import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    account_name: '',
    password: '',
}
export const accountSlice = createSlice({
    name: 'account',
    initialState,
    reducers: {
        addAccount: (state, action) => {
            state.account_name = action.payload
            state.password = action.payload
        },
    },
})

export const { addAccount } = accountSlice.actions
export default accountSlice.reducer
