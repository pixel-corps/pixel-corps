import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"

export const pixelApi = createApi({
    reducerPath: 'pixelApi',
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_API_HOST,
        credentials: "include",
        headers: {
            "Content-Type": "application/json"
        }
    }),
    endpoints: (builder) => ({
        getAllGames: builder.query({
            query: () => ({
                url: '/api/games'
            }),
            providesTags: ['Game']
        }),
        getOneGame: builder.query({
            query: (game_id) => `api/games/${game_id}`
        }),
        createGame: builder.mutation({
            query: ({ name, genre, account_id }) => ({
                url: '/api/games/',
                method: "POST",
                body: { name, genre, account_id },
                credentials: 'include'
            }),
            invalidatesTags: ["Account", "Game"]
        }),
        createIssue: builder.mutation({
            query: ({ title, description, resolved, account_id, game_id }) => ({
                url: '/api/issues/',
                method: "POST",
                body: { title, description, resolved, account_id, game_id },
                credentials: 'include'
            }),
            invalidatesTags: ["Account", "Issue", "GameWithIssues"]
        }),
        getAllIssues: builder.query({
            query: () => ({
                url: '/api/issues'
            }),
            providesTags: ['Issue']
        }),
        getToken: builder.query({
            query: () => ({
                url: '/token',
                credentials: 'include'
            }),
            providesTags: ["Account"]
        }),
        createAccount: builder.mutation({
            query: ({ account_name, password }) => ({
                url: '/api/accounts',
                method: 'POST',
                body: { account_name, password }

            }),
            invalidatesTags: ["Account"]
        }),
        login: builder.mutation({
            query: (info) => {
                const formData = new FormData();
                formData.append('username', info.account_name)
                formData.append('password', info.password)

                return {
                    url: '/token',
                    method: 'POST',
                    body: formData,
                    credentials: 'include',
                    headers: { 'Content-Type': undefined }
                }
            },
            invalidatesTags: ['Account']
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: 'DELETE',
                credentials: 'include'
            }),
            invalidatesTags: ["Account"]
        }),
        getOneIssue: builder.query({
            query: (issue_id) => ({
                url: `/api/issues/${issue_id}`
            }),
            providesTags: ['OneIssue']
        }),
        updateIssue: builder.mutation({
            query: ({ issue_id, title, description, resolved }) => ({
                url: `/api/issues/${issue_id}`,
                method: 'PUT',
                body: { title, description, resolved },
                credentials: 'include'
            }),
            invalidatesTags: ['Account', "Issue", "OneIssue", "GameWithIssues"]
        }),
        deleteIssue: builder.mutation({
            query: (issue_id) => ({
                url: `/api/issues/${issue_id}`,
                method: 'DELETE',
                credentials: 'include'
            }),
            invalidatesTags: ['Account', "Issue", "GameWithIssues"]
        }),
        createComment: builder.mutation({
            query: ({ comment_text, issue_id, account_id }) => ({
                url: '/api/comments',
                method: 'POST',
                body: { comment_text, issue_id, account_id },
                credentials: 'include'
            }),
            invalidatesTags: ['Account', "Comment", "CommentWithIssue"]
        }),
        getOneComment: builder.query({
            query: (comment_id) => ({
                url: `/api/comments/${comment_id}`
            }),
            invalidatesTags: ['Account', "Comment"]
        }),
        updateComment: builder.mutation({
            query: ({ comment_id, comment_text }) => ({
                url: `/api/comments/${comment_id}`,
                method: 'PUT',
                body: { comment_text },
                credentials: 'include'
            }),
            invalidatesTags: ['Account', "Comment", "CommentWithIssue"]
        }),
        deleteComment: builder.mutation({
            query: (commentId) => ({
                url: `/api/comments/${commentId}`,
                method: 'DELETE',
                credentials: 'include'
            }),
            invalidatesTags: ['Account', "CommentWithIssue"]
        }),
        getAllCommentForIssue: builder.query({
            query: (issue_id) => ({
                url: `/api/issues/${issue_id}/comments`
            }),
            providesTags: ["CommentWithIssue"],
            invalidatesTags: ["Account"]
        }),
        getAllIssueForGame: builder.query({
            query: (game_id) => ({
                url: `/api/games/${game_id}/issues`
            }),
            providesTags: ["GameWithIssues"],
            invalidatesTags: ["Account"]
        })



    })
})


export const {
    useGetAllIssueForGameQuery,
    useCreateCommentMutation,
    useCreateGameMutation,
    useCreateIssueMutation,
    useDeleteCommentMutation,
    useCreateAccountMutation,
    useLogoutMutation,
    useLoginMutation,
    useDeleteIssueMutation,
    useGetOneCommentQuery,
    useGetOneGameQuery,
    useGetOneIssueQuery,
    useGetAllGamesQuery,
    useGetAllIssuesQuery,
    useGetTokenQuery,
    useUpdateCommentMutation,
    useUpdateIssueMutation,
    useGetAllCommentForIssueQuery,
} = pixelApi
