import { configureStore } from '@reduxjs/toolkit'
import accountSlice from './accountSlice'
import { pixelApi } from "./apiSlice"
import gameSlice from './gameSlice'
import commentSlice from './commentSlice'
import issueSlice from './issueSlice'

export default configureStore({
  reducer: {
    accountQuery: accountSlice,
    gameQuery: gameSlice,
    commentQuery: commentSlice,
    issueQuery: issueSlice,
    [pixelApi.reducerPath]: pixelApi.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(pixelApi.middleware)
})
