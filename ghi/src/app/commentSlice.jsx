import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    comment_text: '',
}

export const commentSlice = createSlice({
    name: 'comment',
    initialState,
    reducers: {
        addComment: (state, action) => {
            state.comment_text = action.payload
        },
    },
})

export const { addComment } = commentSlice.actions
export default commentSlice.reducer
