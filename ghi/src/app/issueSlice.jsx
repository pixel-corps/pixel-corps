import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    title: '',
    description: '',
    resolved: false,
}

export const issueSlice = createSlice({
    name: 'issue',
    initialState,
    reducers: {
        addIssue: (state, action) => {
            state.title = action.payload
            state.description = action.payload
            state.resolved = action.payload
        },
    },
})

export const { addIssue } = issueSlice.actions
export default issueSlice.reducer
