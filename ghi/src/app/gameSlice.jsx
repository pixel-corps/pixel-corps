import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    name: '',
    genre: '',
}

export const gameSlice = createSlice({
    name: 'game',
    initialState,
    reducers: {
        addGame: (state, action) => {
            state.name = action.payload
            state.genre = action.payload
        },
    },
})

export const { addGame } = gameSlice.actions
export default gameSlice.reducer
