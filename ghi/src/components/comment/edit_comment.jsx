import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import {
    useUpdateCommentMutation,
    useGetTokenQuery,
    useGetOneCommentQuery,
} from '../../app/apiSlice'

function EditComment() {
    const { issue_id } = useParams()
    const { comment_id } = useParams()
    const { data: oldComment, isLoading } = useGetOneCommentQuery(comment_id)
    const [comment_text, setCommentText] = useState()
    const { data: token } = useGetTokenQuery()
    const [update] = useUpdateCommentMutation()
    const navigate = useNavigate()
    useEffect(() => {
        if (isLoading) {
            return
        }
        setCommentText(oldComment.comment_text)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoading])
    if (!token) {
        return (
            <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
                Log in to edit a comment.
            </div>
        )
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const account_id = token.account.account_id
        update({ comment_id, comment_text, account_id })
        navigate(`/issue/${issue_id}`)
    }
    if (isLoading) {
        return <div>Loading...</div>
    }

    return (
        <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
            <div className="w-full max-w-md">
                <h1 className="text-center text-3xl font-bold mb-4">
                    Edit a Comment
                </h1>
                <form
                    onSubmit={handleSubmit}
                    className="bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-4"
                >
                    <div className="bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-6">
                        <div className="mb-6">
                            <label
                                htmlFor="comment_text"
                                className="block text-gray-300 text-sm font-bold mb-2"
                            ></label>
                            <textarea
                                id="comment_text"
                                placeholder="Enter your comment"
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                value={comment_text}
                                onChange={(e) => setCommentText(e.target.value)}
                                rows={4}
                            />
                        </div>
                        <div className="flex items-center justify-center">
                            <button
                                type="submit"
                                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            >
                                Comment
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default EditComment
