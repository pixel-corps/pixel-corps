import { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useCreateCommentMutation } from '../../app/apiSlice'
import { useGetTokenQuery } from '../../app/apiSlice'

function CreateComment() {
    const [comment_text, setCommentText] = useState('')
    const { issue_id } = useParams()

    const { data: token } = useGetTokenQuery()
    const [create] = useCreateCommentMutation()
    const navigate = useNavigate()
    if (!token) {
        return (
            <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
                Log in to create a comment.
            </div>
        )
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const account_id = token.account.account_id
        create({ comment_text, issue_id, account_id })
        navigate(`/issue/${issue_id}`)
    }

    return (
        <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
            <div className="w-full max-w-md">
                <h1 className="text-center text-3xl font-bold mb-4">
                    Leave a Comment
                </h1>
                <form
                    onSubmit={handleSubmit}
                    className="bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-4"
                >
                    <div className="bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-6">
                        <div className="mb-6">
                            <label
                                htmlFor="comment_text"
                                className="block text-gray-300 text-sm font-bold mb-2"
                            >
                                Comment
                            </label>
                            <textarea
                                id="comment_text"
                                placeholder="Enter your comment"
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                value={comment_text}
                                onChange={(e) => setCommentText(e.target.value)}
                                rows={4}
                            />
                        </div>
                        <div className="flex items-center justify-center">
                            <button
                                type="submit"
                                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            >
                                Comment
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default CreateComment
