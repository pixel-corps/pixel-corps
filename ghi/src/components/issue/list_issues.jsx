import { Link } from 'react-router-dom'
import {
    useDeleteIssueMutation,
    useGetAllIssuesQuery,
    useGetTokenQuery,
} from '../../app/apiSlice'
import { useNavigate } from 'react-router-dom'
import Delete from '../../images/delete/delete.png'
import Pencil from '../../images/Edit/Pencil.png'

function IssueList() {
    const { data: issues, isLoading } = useGetAllIssuesQuery()
    const { data: account } = useGetTokenQuery()
    const [deleteMe] = useDeleteIssueMutation()
    const navigate = useNavigate()
    const handleDelete = (issue_id) => {
        deleteMe({ issue_id })
    }
    const editIssue = (issue_id) => {
        navigate(`/issue/${issue_id}/edit`)
    }
    if (!account) {
        return (
            <div className="min-h-screen bg-gray-900 text-white">
                <div className=" bg-gray-900 text-white flex flex-col items-center justify-center">
                    Log in to edit or delete your issues.
                </div>
                <div className="max-w-3xl mx-auto p-4">
                    <h1 className="text-3xl font-bold text-center mb-8">
                        All Issues
                    </h1>
                    <div className="grid grid-cols-1 gap-6">
                        {issues &&
                            [...issues]
                                .sort((a, b) => b.issue_id - a.issue_id)
                                .map((issue) => {
                                    return (
                                        <div
                                            key={issue.issue_id}
                                            className="bg-gray-800 border border-gray-700 rounded-lg shadow-lg transition duration-300 ease-in-out transform hover:shadow-xl hover:scale-105"
                                        >
                                            <Link
                                                to={`/issue/${issue.issue_id}`}
                                                className="block p-4"
                                            >
                                                <h2 className="text-2xl font-bold text-blue-500 hover:text-blue-700 mb-2">
                                                    {issue.title}
                                                </h2>
                                                <p className="text-gray-400 mb-2">
                                                    {issue.description}
                                                </p>
                                            </Link>
                                        </div>
                                    )
                                })}
                    </div>
                </div>
            </div>
        )
    }

    if (isLoading) {
        return <div>Loading...</div>
    }
    return (
        <div className="bg-gray-900 text-white min-h-screen">
            <div className="max-w-3xl mx-auto p-4">
                <h1 className="text-3xl font-bold text-center mb-8">
                    All Issues
                </h1>
                <div className="grid grid-cols-1 gap-6">
                    {issues &&
                        [...issues]
                            .sort((a, b) => b.issue_id - a.issue_id)
                            .map((issue) => {
                                return (
                                    <div
                                        key={issue.issue_id}
                                        className="bg-gray-800 border border-gray-700 rounded-lg shadow-lg transition duration-300 ease-in-out transform hover:shadow-xl hover:scale-105"
                                    >
                                        <Link
                                            to={`/issue/${issue.issue_id}`}
                                            className="block p-4"
                                        >
                                            <h2 className="text-2xl font-bold text-blue-500 hover:text-blue-700 mb-2">
                                                {issue.title}
                                            </h2>
                                            <p className="text-gray-400 mb-2">
                                                {issue.description}
                                            </p>
                                        </Link>
                                        <div>
                                            {issue.account_id ===
                                                account.account.account_id && (
                                                <div className="flex justify-end">
                                                    <button
                                                        onClick={() => {
                                                            editIssue(
                                                                issue.issue_id
                                                            )
                                                        }}
                                                    >
                                                        <img
                                                            src={Pencil}
                                                            width="32"
                                                            height="32"
                                                        />
                                                    </button>
                                                    <button
                                                        onClick={() => {
                                                            handleDelete(
                                                                issue.issue_id
                                                            )
                                                        }}
                                                    >
                                                        <img
                                                            className="right-0"
                                                            src={Delete}
                                                            width="32"
                                                            height="32"
                                                        />
                                                    </button>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                )
                            })}
                </div>
            </div>
        </div>
    )
}

export default IssueList
