import { useParams } from 'react-router-dom'
import Delete from '../../images/delete/delete.png'
import Pencil from '../../images/Edit/Pencil.png'
import { useNavigate } from 'react-router-dom'
import {
    useDeleteCommentMutation,
    useGetTokenQuery,
    useGetOneIssueQuery,
    useGetAllCommentForIssueQuery,
} from '../../app/apiSlice'

function IssuePage() {
    const { data: account } = useGetTokenQuery()
    const { issue_id } = useParams()
    const { data: issues, isLoading: issueLoading } =
        useGetOneIssueQuery(issue_id)
    const { data: comments } = useGetAllCommentForIssueQuery(issue_id)
    const navigate = useNavigate()
    const [deleteMe] = useDeleteCommentMutation()
    const handleDelete = (commentId) => {
        deleteMe(commentId)
    }
    const routeChange = () => {
        navigate(`/comment/create/${issue_id}`)
    }
    const editComment = (commentId) => {
        navigate(`/issue/${issue_id}/comment/${commentId}/edit`)
    }

    if (issueLoading) {
        return <div>Loading...</div>
    }
    if (!account) {
        return (
            <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
                <div className=" bg-gray-900 text-white flex flex-col items-center justify-center">
                    Log In to leave a comment.
                </div>
                <h1 className="text-3xl font-bold mb-8">{issues.title}</h1>
                <textarea
                    className="w-full max-w-3xl h-64 bg-gray-800 text-white border border-gray-700 rounded-lg p-4 mb-8 resize-none"
                    value={issues.description}
                    readOnly
                />

                <div className="w-full">
                    {comments &&
                        comments.map((comment) => {
                            return (
                                <div
                                    key={comment.comment_id}
                                    className="mx-4 md:mx-8 lg:mx-16 xl:mx-32 min-w-96 p-6 mb-4 bg-gray-800 border border-gray-700 rounded-lg shadow-md"
                                >
                                    <p className="mb-2 text-lg font-semibold text-blue-500">
                                        {comment.comment_text}
                                    </p>
                                </div>
                            )
                        })}
                </div>
            </div>
        )
    }

    return (
        <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
            <h1 className="text-3xl font-bold mb-8">{issues.title}</h1>
            <textarea
                className="w-full max-w-3xl h-64 bg-gray-800 text-white border border-gray-700 rounded-lg p-4 mb-8 resize-none"
                value={issues.description}
                readOnly
            />

            <div className="w-full">
                <button
                    className="ml-32 mb-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    onClick={routeChange}
                >
                    Leave a Comment
                </button>
                {comments &&
                    comments.map((comment) => {
                        return (
                            <div
                                key={comment.comment_id}
                                className="mx-4 md:mx-8 lg:mx-16 xl:mx-32 min-w-96 p-6 mb-4 bg-gray-800 border border-gray-700 rounded-lg shadow-md"
                            >
                                <p className="mb-2 text-lg font-semibold text-blue-500">
                                    {comment.comment_text}
                                </p>
                                <div>
                                    {comment.account_id ===
                                        account.account.account_id && (
                                        <div className="flex justify-end">
                                            <button
                                                onClick={() => {
                                                    editComment(
                                                        comment.comment_id
                                                    )
                                                }}
                                            >
                                                <img
                                                    src={Pencil}
                                                    width="32"
                                                    height="32"
                                                />
                                            </button>
                                            <button
                                                onClick={() => {
                                                    handleDelete(
                                                        comment.comment_id
                                                    )
                                                }}
                                            >
                                                <img
                                                    className="right-0"
                                                    src={Delete}
                                                    width="32"
                                                    height="32"
                                                />
                                            </button>
                                        </div>
                                    )}
                                </div>
                            </div>
                        )
                    })}
            </div>
        </div>
    )
}

export default IssuePage
