import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import {
    useUpdateIssueMutation,
    useGetTokenQuery,
    useGetOneIssueQuery,
} from '../../app/apiSlice'

function EditIssue() {
    const { issue_id } = useParams()
    const { data: oldIssue, isLoading } = useGetOneIssueQuery(issue_id)
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [resolved, setResolved] = useState(false)
    const { data: token } = useGetTokenQuery()
    const [update] = useUpdateIssueMutation()
    const navigate = useNavigate()
    useEffect(() => {
        if (isLoading) {
            return
        }
        setTitle(oldIssue.title)
        setDescription(oldIssue.description)
        setResolved(oldIssue.resolved)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoading])
    if (!token) {
        return (
            <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
                Log In to edit an issue.
            </div>
        )
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const account_id = token.account.account_id
        update({ issue_id, title, description, resolved, account_id })
        navigate(`/issue`)
    }
    if (isLoading) {
        return <div>Loading...</div>
    }

    return (
        <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
            <div className="w-full max-w-md">
                <h1 className="text-center text-3xl font-bold mb-4">
                    Edit an Issue
                </h1>
                <form
                    onSubmit={handleSubmit}
                    className="bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-4"
                >
                    <div className="bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-6">
                        <div className="mb-6">
                            <label
                                htmlFor="title"
                                className="block text-gray-300 text-sm font-bold mb-2"
                            ></label>
                            <input
                                id="title"
                                type="text"
                                placeholder=""
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                            />
                        </div>
                        <div className="mb-6">
                            <label
                                htmlFor="description"
                                className="block text-gray-300 text-sm font-bold mb-2"
                            ></label>
                            <textarea
                                id="description"
                                placeholder="Enter your comment"
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                value={description}
                                onChange={(e) => setDescription(e.target.value)}
                                rows={4}
                            />
                        </div>
                        <div className="mb-6">
                            <label
                                htmlFor="resolved"
                                className="block text-gray-300 text-sm font-bold mb-2"
                            >
                                Resolved?
                            </label>
                            <input
                                id="resolved"
                                type="checkbox"
                                placeholder="Enter your comment"
                                checked={resolved}
                                onChange={(e) => setResolved(e.target.checked)}
                            />
                        </div>
                        <div className="flex items-center justify-center">
                            <button
                                type="submit"
                                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            >
                                Update
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default EditIssue
