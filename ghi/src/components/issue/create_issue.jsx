import { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useCreateIssueMutation } from '../../app/apiSlice'
import { useGetTokenQuery } from '../../app/apiSlice'

function CreateIssue() {
    const [title, setTitle] = useState('')
    const { game_id } = useParams()
    const [description, setDescription] = useState('')
    const [resolved, setResolved] = useState(false)
    const { data: token } = useGetTokenQuery()
    const [create] = useCreateIssueMutation()
    const navigate = useNavigate()
    if (!token) {
        return (
            <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
                Log In to create an Issue.
            </div>
        )
    }

    const handleSubmit = async (e) => {
        const account_id = token.account.account_id
        e.preventDefault()
        create({ title, description, resolved, account_id, game_id })
        navigate(`/game/${game_id}`)
    }

    return (
        <div className="min-h-screen bg-gray-900 text-white flex items-center justify-center">
            <div className="w-full max-w-xs">
                <div>
                    <h1 className="text-center text-3xl font-bold mb-4">
                        Create an Issue
                    </h1>
                </div>
                <form
                    onSubmit={handleSubmit}
                    className="bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-4"
                >
                    <div className="mb-4">
                        <label
                            className="block text-gray-300 text-sm font-bold mb-2"
                            htmlFor="title"
                        >
                            Title
                        </label>
                        <input
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="title"
                            placeholder="Title"
                            type="text"
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                        />
                    </div>
                    <div className="mb-4">
                        <label
                            className="block text-gray-300 text-sm font-bold mb-2"
                            htmlFor="description"
                        >
                            Description
                        </label>
                        <input
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="description"
                            placeholder="Description"
                            type="text"
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                        />
                    </div>
                    <div className="mb-4">
                        <label className="flex items-center text-gray-300 text-sm font-bold">
                            <input
                                className="mr-2 leading-tight"
                                id="resolved"
                                type="checkbox"
                                checked={resolved}
                                onChange={(e) => setResolved(e.target.checked)}
                            />
                            <span>Resolved?</span>
                        </label>
                    </div>
                    <div className="flex items-center justify-center">
                        <button
                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            Create
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default CreateIssue
