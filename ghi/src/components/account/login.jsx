import { useState } from 'react'
import { useLoginMutation } from '../../app/apiSlice'
import { useNavigate } from 'react-router-dom'

function Login() {
    const [account_name, setAccountName] = useState('')
    const [password, setPassword] = useState('')
    const [login] = useLoginMutation()
    const navigate = useNavigate()

    const handleSubmit = async (e) => {
        e.preventDefault()
        login({ account_name, password })
        navigate('/')
    }

    return (
        <div className="min-h-screen bg-gray-900 text-white flex items-center justify-center">
            <div className="w-full max-w-xs">
                <h1 className="text-center text-3xl font-bold mb-4">Login</h1>
            </div>
            <form
                onSubmit={handleSubmit}
                className="bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-4"
            >
                <div className="mb-4">
                    <label
                        className="block text-gray-300 text-sm font-bold mb-2"
                        htmlFor="account_name"
                    >
                        Account Name
                    </label>
                    <input
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="account_name"
                        placeholder="Account Name"
                        type="text"
                        value={account_name}
                        onChange={(e) => setAccountName(e.target.value)}
                    />
                </div>
                <div className="mb-6">
                    <label
                        className="block text-gray-300 text-sm font-bold mb-2"
                        htmlFor="password"
                    >
                        Password
                    </label>
                    <input
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="password"
                        placeholder="******************"
                        type="text"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                <div className="flex items-center justify-center">
                    <button
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        type="submit"
                    >
                        Sign In
                    </button>
                </div>
            </form>
        </div>
    )
}

export default Login
