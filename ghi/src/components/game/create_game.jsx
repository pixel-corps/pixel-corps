import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useCreateGameMutation } from '../../app/apiSlice'
import { useGetTokenQuery } from '../../app/apiSlice'

function CreateGame() {
    const [name, setName] = useState('')
    const [genre, setGenre] = useState('')
    const [create] = useCreateGameMutation()
    const { data: token } = useGetTokenQuery()
    const navigate = useNavigate()
    if (!token) {
        return (
            <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
                Login to create a game.
            </div>
        )
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const account_id = token.account.account_id
        create({ name, genre, account_id })
        navigate('/game')
    }

    return (
        <div className="min-h-screen bg-gray-900 text-white flex items-center justify-center">
            <div className="w-full max-w-xs">
                <div>
                    <h1 className="text-center text-3xl font-bold mb-4">
                        Create a Game
                    </h1>
                </div>
                <form
                    onSubmit={handleSubmit}
                    className="bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-4"
                >
                    <div className="mb-4">
                        <label
                            className="block text-gray-300 text-sm font-bold mb-2"
                            htmlFor="name"
                        >
                            Game Name
                        </label>
                        <input
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="name"
                            placeholder="Name"
                            type="text"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                    </div>
                    <div className="mb-4">
                        <label
                            className="block text-gray-300 text-sm font-bold mb-2"
                            htmlFor="genre"
                        >
                            Genre
                        </label>
                        <input
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="genre"
                            placeholder="Genre"
                            type="text"
                            value={genre}
                            onChange={(e) => setGenre(e.target.value)}
                        />
                    </div>
                    <div className="flex items-center justify-center">
                        <button
                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default CreateGame
