import { Link } from 'react-router-dom'
import { useGetAllGamesQuery } from '../../app/apiSlice'

function GameList() {
    const { data: games, isLoading } = useGetAllGamesQuery()

    if (isLoading) {
        return (
            <div className="flex items-center justify-center h-screen">
                Loading...
            </div>
        )
    }

    return (
        <div className="bg-gray-900 text-white min-h-screen">
            <div className="max-w-3xl mx-auto p-4">
                <h1 className="text-3xl font-bold text-center mb-8">Games</h1>
                <div className="grid grid-cols-1 gap-6">
                    {games &&
                        games.map((game) => {
                            return (
                                <div
                                    key={game.game_id}
                                    className="bg-gray-800 border border-gray-700 rounded-lg shadow-lg transition duration-300 ease-in-out transform hover:shadow-xl hover:scale-105"
                                >
                                    <Link
                                        to={`/game/${game.game_id}`}
                                        className="block p-4"
                                    >
                                        <h2 className="text-2xl font-bold text-blue-500 hover:text-blue-700 mb-2">
                                            {game.name}
                                        </h2>
                                        <p className="text-gray-400 mb-2">
                                            {game.genre}
                                        </p>
                                    </Link>
                                </div>
                            )
                        })}
                </div>
            </div>
        </div>
    )
}

export default GameList
