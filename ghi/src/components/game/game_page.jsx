import { useParams } from 'react-router-dom'
import Delete from '../../images/delete/delete.png'
import Pencil from '../../images/Edit/Pencil.png'
import { useNavigate } from 'react-router-dom'
import {
    useGetOneGameQuery,
    useGetAllIssueForGameQuery,
    useDeleteIssueMutation,
    useGetTokenQuery,
} from '../../app/apiSlice'

function GamePage() {
    const { data: account } = useGetTokenQuery()
    const { game_id } = useParams()
    const { data: game, isLoading: gameLoading } = useGetOneGameQuery(game_id)
    const { data: issues } = useGetAllIssueForGameQuery(game_id)
    const navigate = useNavigate()
    const [deleteMe] = useDeleteIssueMutation()
    const routeChange = () => {
        navigate(`/issue/create/${game_id}`)
    }
    const handleDelete = (issue_id) => {
        deleteMe(issue_id)
    }
    const editIssue = (issue_id) => {
        navigate(`/issue/${issue_id}/edit`)
    }
    if (gameLoading) {
        return <div>Loading...</div>
    }

    return (
        <div className="min-h-screen bg-gray-900 text-white flex flex-col items-center justify-center">
            <h1 className="text-3xl font-bold mb-8">{game.name}</h1>
            <textarea
                className="w-full max-w-3xl h-64 bg-gray-800 text-white border border-gray-700 rounded-lg p-4 mb-8 resize-none"
                value={game.genre}
                readOnly
            />
            <div className="w-full">
                <button
                    className="ml-32 mb-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    onClick={routeChange}
                >
                    Add an Issue
                </button>
                {issues &&
                    issues.map((issue) => {
                        return (
                            <div
                                key={issue.issue_id}
                                className="mx-4 md:mx-8 lg:mx-16 xl:mx-32 min-w-96 p-6 mb-4 bg-gray-800 border border-gray-700 rounded-lg shadow-md"
                            >
                                <h5 className="mb-2 text-lg font-semibold text-blue-500">
                                    {issue.title}
                                </h5>
                                <p className="mb-2 text-lg font-semibold text-blue-500">
                                    {issue.description}
                                </p>
                                <div>
                                    {issue.account_id ===
                                        account.account.account_id && (
                                        <div className="flex justify-end">
                                            <button
                                                onClick={() => {
                                                    editIssue(issue.issue_id)
                                                }}
                                            >
                                                <img
                                                    src={Pencil}
                                                    width="32"
                                                    height="32"
                                                />
                                            </button>
                                            <button
                                                onClick={() => {
                                                    handleDelete(issue.issue_id)
                                                }}
                                            >
                                                <img
                                                    className="right-0"
                                                    src={Delete}
                                                    width="32"
                                                    height="32"
                                                />
                                            </button>
                                        </div>
                                    )}
                                </div>
                            </div>
                        )
                    })}
            </div>
        </div>
    )
}

export default GamePage
