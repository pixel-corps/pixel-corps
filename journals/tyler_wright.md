# Pixel Corps – Tyler’s Log

## 1/16/2024

We’re off to the races!

_Pixel Corps is an opportunity for game developers to build collaborative networks, help others resolve their longstanding dev issues, seek assistance from other devs, and share success. Corpsmembers gain in rank as they troubleshoot peer projects. We are striving to promote a game dev culture of lifelong learning and peer-to-peer collaboration._

**Why this matters:** Builds credibility, visibility, experience, acumen and professional network

After basking a bit in our momentary high from our wireframe presentation, we quickly sobered up with the daunting task of setting up a functioning FastAPI with a PostgresSQL database—a first for all three of us! I will admit that I felt like more of a spectator than a helper as Hunter and Rachel tag-teamed the technical requirements of Gitlab, establishing configurations, and getting the application to run through Docker.

Rachel encountered a roadblock with running Docker, likely due to some differences with Linux users. The SEIRs came in to save the day and I quickly realized that I would be one chef too many in this troubleshooting. At the time of this writing, they are still troubleshooting—it is remarkable to see the care and attention everybody has toward a fellow SWE. We’re all in this together! But as it stands, I’m not maximizing the use of my time, and so it was the perfect opportunity to focus elsewhere.

Hunter recommended that I write some pseudocode for our SQL database schema, and this proved to be a wonderful exercise in exploring SQL data types, null values, primary keys, references, etc. I also learned a lot about specific best practices in database schema design, such as how to handle user data types like emails and passwords. At this stage in our project, I’m looking to support anywhere that I can; to include working offline so that when the time comes for database work we’ll at the very least have a structure in place.

This project already had its first breath of air and is beginning to resemble something of a living entity. As we speak through the user experience, the project appears to slowly come to life—some elements that we planned for no longer seem practical. Part of the process.

## 1/17/2024

I started the day by starting an authenticator.py file which will eventually support user authentication, being mindful to keep my code commented out for now until we feel reasonably confident that it will work correctly. I also created a pool.py file, beefed out our routers folder with user, game, issue, and comment .py documents--giving some structure to our app. The hope with these efforts is to help us organize our work and begin to see all the moving pieces. Finally, I rounded out the early morning by adding to the docker-compose.yaml file--things like adding a database url and signing key.

Hunter and Rachel troubleshooted some sudden Docker instability while I brainstormed all of the possible routes, the nature of the data (what is visible, what is a required field, what triggers archiving, etc.). Eventually we set our sights on tackling our SQL tables.

The remainder of the day really had us tag-teaming our SQL schema and queries. Hunter led the charge on this, manipulating the data models that we brainstormed the day prior into something that is useable. We are beginning to really see some backend progress! The online resource db-fiddle.com allowed us to test our schema and queries. We are able to create users, games, etc. and perform a few other more complex tasks like retrieval. Hunter and our SEIR Stesha came up with a "bridging tables" solution to solve some of our woes. I will admit that further reading is absolutely required on my end... I am meeting my level of database competence and it's painful.

## 1/18/2024

I’m starting to notice a trend that most of my best coding comes from late nights in solitude. While this naturally puts me at a slight disadvantage come time for working in collaborative projects, I do see opportunity to serve in more of a support capacity during the day.

I was able to put in some work independently on a few structural components of our apps backend, namely: pool.py, the queries folder, adding routers to main, adding a few models (commented out for the time being) to models.py, and adding a few changes to user.py (also commented out). The idea here was to get a little bit ahead of the group by laying down some missing pieces so that we might gradually wrap our heads around the big picture and populate them with beautiful, functional code. These updates were merged into the main branch in the early morning, and then I receded back into a support role for the remainder of the day—which is fine by me, for the record!

A good bulk of the day was following the breadcrumbs that our instructors left behind during lectures and seeing how we might adopt their solutions into our own authentication protocol. Rachel and Hunter took turns coding and merging updates into the main branch while I tried to keep up and chime in where I was able. Having three sets of eyes on code as it is being written really has its advantages! I was impressed with my team members and their ability to quickly identify ways to troubleshoot errors. This level of resourcefulness and confidence, despite being in unfamiliar territory, reminded me that we truly have adopted developer mindsets that will serve us well beyond our time at Hack Reactor.

## 1/19/2024

**SUCCESS!** We've managed to implement user authentication and sign-in tokens! Today was pretty much all three of us working together, throwing every possible solution and line of code at the problem in real-time. Hunter was the one typing and Rachel and I were there to help problem-solve, belting out possible solutions. We spent a lot of time consulting instructor lectures, FastAPI and Postgres documentation, pgAdmin and Swagger dashboards, as well as applying the developer mindset and what we already know to remedy the errors. This was a brilliant way to end the week!

## 1/22/2024

Today was a beast. We spent the entire day working on account routes and queries, managing to get account deletion and account retrieval working but stopping short of getting our update account profile to work. We found ourselves in an endless loop of troubleshooting one item, solving another, and then return back to the original error. Our hope is to resolve this one issue before moving on to building out routes for our games, issues, and comments.

This was the first full day of me coding while the others provided guidance. I do enjoy being in the coder's seat as I gain more familiarity in Postgres through practice.

## 1/23/2024

We managed to get our update account function (PUT request) working. With all the account routes figured out, we moved onto developing Game routes for the remainder of the day, alternating between drivers/navigators.

## 1/24/2024

Finished 'Games' and 'Issues' backend routes.

## 1/25/2024

Hunter and I completed the 'Comments' routes. Tomorrow will likely be a study catch-up day.

## 1/26/2024

Today we worked on developing test cases. I took this time to learn as much as possible so that I might implement my own.
