## January 16 2024

Today I set up all of my settings and pushed it to main

## January 17 2024

Today we all worked on our SQL tables, we were unable to get it set up

## January 18 2024

Today we worked on the SQL tables again and were unable to get it done correctly

## January 19 2024

We finally finished the SQL tables and started on Auth and Account

## January 22 2024

We helped Hunter finally finish Auth and get started on creating accounts. We also started the Update method for account

## January 23 2024

We helped Tyler make get one account, put account and got started on Create Game and Get all games

## January 24 2024

Today we finished Getting all games, getting one game, updating, and delete a game and got started on Issues

## January 25 2024

Was gone from class today so there was nothing I did

## January 26 2024

Today me and Hunter set up some python Unit Tests and that was that

## January 29 2024

Began work on Front-End Auth with Hunter.

## January 30 2024

Hunter began learning about Redux while I started to configure what our website would look like

## January 31 2024

Its down to me and Hunter, We are now able to create an account and the cookie now caches into the database from the frontend

## February 1 2024

Today Hunter worked on the logout function and implemented it while I got the login function working. It was a successful day.

## February 6 2024

Today me and Hunter worked on implementing games and started on issues

## February 7 2024

Today me and Hunter finished up Issues and got Comments to start working.

## February 8 2024

Today me and Hunter started finishing up the website and getting ready for deployment

## February 9 2024

Today is the final day of the project. I focused my work on cleaning up the code and removing things that weren't necessary while Hunter worked on the README.
