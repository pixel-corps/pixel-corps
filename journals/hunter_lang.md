## 1/16

Worked on setting up the database and connecting the back end to that. Spent from 4 until 8 debugging rachels set up

## 1/17

Started working on auth but thought we might need to make an accounts table to get that to work

## 1/18

Worked with Rachel to finalize the SQL and database creation and helped her get it running. Then actually began AUTH. Got up to the point of writing get and create functions. I think those work but im not positive.

## 1/19

Finished auth and implemented the GET Token method

## 1/22

Started the rest of CRUD for accounts. Wrote Get an account. Navigated Tyler through PUT and struggled to get it to work correctly

## 1/23

I finished the PUT for accounts. Began working on GET and POST for Games. Had to stop once we got to testing GET to create game data using post. Finished the create game and got it working but it turns out the bug in GET is not related I don't think. We are still getting a list index out of range.

## 1/24

Began the day by finishing the requests for Games navigating for Rachel. Then began working on issues navigating for tyler. The team finally had a discussion about the foregin keys in the project and how they will operate. During this discussion it was decided that our tables should be updated to no longer use alter table statements and instead add the keys in the create table statement. I rewrote the schema to fix that and then after class I implimented the schema and changed the syntax in the queries. Now it is required to have ids for foreign keys.

## 1/25

I set up Tyler's database with the changes I made last night, then navigated him through creating a comment. He got sick so them I finished the rest of the comments routes on my own. After finishing that I did some readings and research on table joins and unit testing.

## 1/26

Began the day by reviewing Unittesting and added a unittest for the get one account endpoint. Spent the evening working with Rachel and Tyler on their unittests.

## 1/29

Finished Tyler's unittest and then Rachel and I began setting react and redux

## 1/30

Worked through redux. Still learning how it works. Began by building the slice and store and then setup the page for our queries and endpoints

## 1/31

Began work on front end auth. It its now officially just rachel and I so, with the help of bart we began to understand how redux works. We now will work on building out the barebones xml for all of out components.

## 2/1

Finished up front end auth and had a few bugs. We debugged an issue with the content-type for the login form with the help of bart and began filling out the xml for the other MVP components

## 2/5

We added in a ton of html to components so we had a baseline to start working from. We started game and issue pages which is a little tough but we will finish tomorrow

## 2/6

We finished working on the game and issue pages for the front end which has started coming a bit easier than yesterday. We then managed to finish most of the components for our modified mvp. Honestly its a bit sad that this project is meant for four and we only have two developers but we are doing the best that we can. I spent the evening working on the tailwinds for basically every page since rachel had to head out early

## 2/7

Rachel and I thought we were going to finish up the front end and deploy but we were told that since there is only two of us we should focus more on building out our application and less on deployment and save that for next week. We then worked on the tailwind design, adding a comment button on the issue page, created and added a logo in the nav, updated our navigation, and finished by adding a delete icon to the comments on an issue


### 2/8
Spent the moring helping another group with front end auth and then we added edit to comments in the issue page. Then we added delete and edit to issues.

## 2/9
We cleaned up the code, added correct error handling and updated the README