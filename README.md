# Pixel Network

<a name="readme-top"></a>

[![Issues][issues-shield]][issues-url]
Hunter's [![Hunter's LinkedIn][linkedin-shield]][linkedin-url]
Rachel's [![Rachel's LinkedIn][linkedin-shield]][Rlinkedin-url]

![Loogo](ghi/src/images/logo/Logo.png)

  <p align="center">
    Pixel Network provides a dedicated space for game developers to build collaborative networks, help others resolve their longstanding dev issues, seek assistance from other devs, and share success. Members can create games, from which they may post an Issue that is realated to one of those games. Members can also comment on others issue posts to help solve problems.

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#user-stories">User Stories</a></li>
    <li>
        <a href="#deliverables">Deliverables</a>
        <ul>
            <li><a href="#wireframes">Wireframes</a></li>
        </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contact">Contact</a></li>

  </ol>
</details>

## About The Project

GitLab Profiles:`hunterlang44` `rachelallen1310`

Project Name: `Pixel Network`

Email: `hunterlang44@gmail.com` `rachelallen1310@gmail.com`

Description: `Pixel Network is the brainchild of a collaborative effort between Rachel and Hunter, driven by their shared passions and aspirations. Rachel's deep-seated love for video games and her unwavering dedication to the industry merged seamlessly with Hunter's vision to support emerging artists, culminating in the creation of Pixel Network. Welcome to our dynamic and inclusive community, where indie game developers converge to exchange expertise, seek guidance, and draw inspiration for their projects. Serving as a bustling nexus for developers at every stage of their journey, our platform provides a nurturing space for collaboration, troubleshooting technical challenges, and navigating the intricacies of game development. Whether you're a seasoned veteran or just embarking on your creative odyssey, come join us and immerse yourself in a community devoted to nurturing creativity and fostering innovation within the indie game development landscape.`

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Built With

<ul>
    <li>
        <a href="https://fastapi.tiangolo.com/">
            <img src="./Images/FastApiLogo.png" width="200" height="100" />
        </a>
    </li>
    <li>
        <a href="https://react.dev/">
            <img src="./Images/React.png" width="200" height="100"/>
        </a>
    </li>
    <li>
        <a href="https://redux-toolkit.js.org/">
            <img src="./Images/Redux.jpg" width="200" height="100"/>
        </a>
    </li>
    <li>
        <a href="https://tailwindcss.com/">
            <img src="./Images/TailwindCSSLogo.png" width="300" height="65"/>
        </a>
    </li>
    <li>
        <a href="https://vitejs.dev/">
            <img src="./Images/ViteLogo.png" width="100" height="100">
        </a>
    </li>
    <li>
        <a href="https://www.docker.com/">
            <img src="./Images/DockerLogo.png" width="200" height="100">
        </a>
    </li>
</ul>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Getting Started

To get a local copy up and running follow these simple example steps.

### Installation

###### Make sure you have Docker, Git, and Node.js 18.2 or above

1. Fork and Clone the repository
    ```sh
    git clone https://gitlab.com/pixel-corps/pixel-corps
    ```
2. Build Volumes
    ```sh
    docker volume create pg-admin
    docker volume create jwtdown-db-data
    docker volume create postgres-data
    ```
3. Build and Run Containers `config.js`
    ```sh
    docker-compose up --build
    ```
4. Setup .env file
    ```
    Example Provided in example.env.txt file
    ```

-   After running these commands, make sure that all of your Docker Containers are running

-   View the project in the browser at: "hhtp://localhost:5173/

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## User Stories

Feature: Create Issue
In order to solve an issue
Users should be able to
create an issue

Scenario: Create Issue
Given that I have an issue for a game that I cannot solve
And I have an account on Pixel Network
When I create an issue
Then I should be able to post the issue and recieve helpful comments

Feature: Create Comment
In order for issues to be solved
Users should be able to
create comments on issues

Scenario: Create Comment
Given that I have knowledge about how to potentially solve an issue
And I have an account on Pixel Network
When I see and issue I might know the answer to
Then I should be able to post the comment

Feature: Create Account
In order for users to have their issues solved
Users should be able to
create an account

Scenario: Create Account
Given that I have an issue for a game that I cannot solve
And I see the issues and comments on Pixel Network
When I create an account
Then I should be able to create games and issues

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Deliverables

### Wireframes

![Logged In](ghi/src/images/Wireframes/LoggedInHome.png)

![NewIssue](ghi/src/images/Wireframes/NewIssue.png)

![NewUserHome](ghi/src/images/Wireframes/NewUserHome.png)

![ProfileScreenOtherUser](ghi/src/images/Wireframes/ProfileScreenOtherUsers.png)

![QuestionPageForDifferentUser](ghi/src/images/Wireframes/QuestionPageForDifferentUser.png)

![UserQuestionPage](ghi/src/images/Wireframes/UserQuestionPage.png)

![YourComments](ghi/src/images/Wireframes/YourComments.png)

![YourIssuePage](ghi/src/images/Wireframes/YourIssuePage.png)

![YourIssues](ghi/src/images/Wireframes/YourIssues.png)

##### GitLab issue board is setup and in use

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Roadmap

-   [ ] Set Up Docker Compose and Database
-   [ ] Connect Vite to Backend using env file
-   [ ] Developing a Database Schema
-   [ ] Setting up gi
-   [ ] Auth Setup
-   [ ] Auth
    -   [ ] Auth and main
-   [ ] Reworking postgres schema
-   [ ] Account GET, PUSH, DELETS
    -   [ ] Fix DELETE Account
-   [ ] GET all Games
-   [ ] Create Game
-   [ ] Finishing all games
-   [ ] Beginning Issues routes
-   [ ] Rework Foreign Key Design
    -   [ ] Debugging Database schema and queries change
-   [ ] Comment routes
    -   [ ] Finishing comment routes
-   [ ] Rachel Unittest
-   [ ] Tyler Unittest
-   [ ] Hunter Unittest
-   [ ] Vite Set up
-   [ ] Starting Home Page
-   [ ] First slice Creation
-   [ ] xml for components
-   [ ] Slices and queries
-   [ ] Components html
-   [ ] Finishing slices for comment and issue
-   [ ] connecting redux and create game and issue
-   [ ] create comment and adding to nav
-   [ ] styling game list
-   [ ] Creating Issue Pages and working with Tailwind
    -   [ ] Making tailwinds look more professional
-   [ ] Comment button on Issue page
-   [ ] Logo
-   [ ] Adding items to nav
-   [ ] Delete comment on issue page
-   [ ] IssueAdding edit and delete to games and issues
-   [ ] readme
-   [ ] Cleaning up code

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Functionality

Pixel Network empowers users with comprehensive CRUD functionalities, enabling seamless management of accounts, games, issues, and comments. Upon server activation, users can effortlessly navigate to FastAPI Swagger at 'http://localhost:8000/docs#/' to interact with the server through requests. This MVP prioritizes core issue-solving aspects in game development, with our front-end showcasing meticulously integrated CRUD pathways. From account creation to comment manipulation, including logging in, logging out, game and issue creation, as well as editing and deletion of issues and comments, Pixel Network encapsulates these pivotal features. While these actions represent the cornerstone of Pixel Network's functionality within the constraints of our current timeframe, our roadmap eagerly anticipates the incorporation of all available request functionalities in FastAPI Swagger.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Testing

In the '/api/Tests/' directory, you'll find three meticulously crafted unit tests. The first evaluates the retrieval of a single comment, followed by a test examining the retrieval of an individual issue. Lastly, there's a test focusing on fetching a single user. Hunter created the test for retrieving a user, while Rachel tackled the test for fetching an issue. Both tests follow a similar structure: they initiate by generating mock queries and data, which are then fed into the router to execute the corresponding 'get one' method. Success is determined by comparing the returned results to the provided mock data. If the returned data matches the expected mock data, the test triumphs; otherwise, it signals a failure.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contact

Hunter Lang - hunterlang44@gmail.com
Rachel Allen - RachelAllen1310@gmail.com

Project Link: [https://gitlab.com/pixel-corps/pixel-corps](https://gitlab.com/pixel-corps/pixel-corps)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: https://gitlab.com/pixel-corps/pixel-corps/-/issues
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/hunter-lang-3aa1871b3/
[Rlinkedin-url]: https://www.linkedin.com/in/rachel-allen-024666247/
[Fast.js]: https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png
[Fast-url]: https://fastapi.tiangolo.com/
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[RTK Query.js]: https://redux-toolkit.js.org/img/redux.svg
[RTK-url]: https://redux-toolkit.js.org/
[Tailwinds.com]: https://tailwindcss.com/_next/static/media/tailwindcss-logotype-white.944c5d0ef628083bb316f9b3d643385c86bcdb3d.svg
[Tailwinds-url]: https://tailwindcss.com/
